package schema


type Enc_t struct {
	Field  string
	Value  string
	ID     string
	Author string
}

type Dec_t struct {
	Field  string
	Hide   bool
	ID     string
	Author string
}

type Res_t struct {
	Field  string
	Hide   bool
	ID     string
}
